# Comparision between Julien and Matthieu-Samah results

This markdown paper is used to compare the results obtained between two Merge Request extraction tools from the company Kaloom.

Kaloom is an IT company with different functional groups (CP, DP, PF, MP, PF, FPGA). When a developer finishes programming, reviewers validate these new lines of code. These are called merge requests.

These merge requests have an opening date (when the code begins to be parsed) and they are closed when the code is accepted or rejected. There may be several days between these two dates.

The data from merge requests is intended to be extracted and analyzed.

Two versions of an extraction tool have been developed. The first version (V1) was developed by Julien Legault. It was executed to extract all Merge Requests from Kaloom with a creation date in 2020 until August 12, 2021.
For the second version, Samah Kansab and Matthieu Hanania updated the first version to implement new metrics. It was executed on all merge requests from 2020 and 2021.

To check the proper functioning of the new tool, it is interesting to compare the results between the two versions.


## 1. Comparing the dates for CP and MP groups 

A first idea to compare the results is to check if the creation and closing dates of merge requests are the same. To do this, we can start by comparing the results for two groups and two different years.

To facilitate the comparison process, we can store the results obtained into Excel files, which will help us identify the differences effectively.

In each Excel file, let's say "Comparison_2020-cp_V1V2.xlsx," there will be two sheets: "V1," which contains Julian's results, and "V2," which contains Samah and Matthew's results. This arrangement will allow us to easily compare the outcomes between the two versions.

By examining these Excel files and comparing the data in the two sheets, we can analyze any discrepancies or variations in the creation and closing dates of merge requests. This approach will give us valuable insights into how the new metrics implemented by Samah and Matthieu in the second version affect the results compared to Julian's original version.


### 1.1 Comparing the creation date

Both versions extract the merge request creation date. The field is called "local_creation_date" because these dates are converted to Canadian time.

By comparing the *"local_creation_date"* from V2 to the *"Creation Date"* from V1, we can observe that both versions extract the same date.

| Projet | MR  | local_creation_date|Creation Date       |
|--------|-----|--------------------|--------------------|
| 88     | 306 | 2020-10-01 10:26:43|2020-10-01 10:26:43 |


### 1.2 Comparing the end date

The differences that may appear are at the level of the closing date of the merge requests (called "end date"). Indeed, since the first version of the tool did not collect all the data from 2021, and considering that the second version is much more recent, some merge requests have been closed.

By comparing the *"local_end_date"* from V2 to the *"End Date"* from V1, we can observe that some projects are newly closed.

**2020-CP** : List of Merge Requests that are newly closed (were still open in V1):

| Projet | MR  | Canadian closing date  | Closing Type (merged/closed)|
|--------|-----|------------------------|-----------------------------|
| 874    | 247 | 2022-04-07 14:55:01    |      merged                 |
| 876    | 190 | 2022-02-11 11:57:21    |      closed                 |
| 877    | 129 | 2022-02-11 12:00:49    |      merged                 |
| 1125   | 129 | 2022-06-20 11:37:56    |      closed                 |
| 1126   | 194 | 2022-07-20 20:55:46    |      closed                 |
| 1126   | 314 | 2022-02-11 11:52:27    |      closed                 |


**2021-CP**: List of Merge Requests that are newly closed (were still open in V1):

| Projet | MR   | Canadian closing date  | Closing Type (merged/closed)|
|--------|------|------------------------|-----------------------------|
| 88     | 392  | 2022-05-02 08:53:02    |      closed                 |
| 156    | 435  | 2022-05-02 08:54:51    |      closed                 |
| 785    | 954  | 2022-05-02 08:31:18    |      closed                 |
| 873    | 208  | 2022-05-02 08:51:39    |      closed                 |
| 874    | 474  | 2022-01-26 11:43:22    |      closed                 |
| 913    | 207  | 2022-02-05 08:47:57    |      closed                 |
| 1122   | 265  | 2022-02-05 08:45:56    |      closed                 |
| 1126   | 402  | 2022-02-05 08:38:29    |      closed                 |
| 1147   | 383  | 2022-02-05 08:46:38    |      closed                 |
| 1335   | 435  | 2022-02-05 08:52:27    |      closed                 |
| 1446   | 134  | 2022-02-05 08:52:13    |      closed                 |
| 1584   | 103  | 2022-02-05 08:29:30    |      closed                 |

**2020-MP** : no projects are newly closed in V2

**2021-MP** : List of Merge Requests that are newly closed (were still open in V1):

| Projet | MR   | Canadian closing date  | Closing Type (merged/closed) |
|--------|------|------------------------|------------------------------|
| 153    | 1250 | 2022-02-05 10:58:00    | closed                       |
| 172    | 111  | 2022-04-07 16:25:00    | closed                       |
| 806    | 1047 | 2022-02-05 10:58:00    | closed                       |
| 850    | 611  | 2022-02-05 10:58:00    | closed                       |
| 882    | 1194 | 2022-02-05 10:59:00    | closed                       |


**Interesting points** :The most common date in newly closed Merge Requests appears to be May 2, which requires further investigation.

This is an intriguing comparison as it emphasizes that the differences in dates are a result of the closure of merge requests.

Additionally, we observe that the majority of these merge requests, which were closed recently (in 2022), were not accepted.
  
## 2. Comparing the number of Merge Request for each group

By comparing the merge requests of the two extraction tools, we can determine if they extracted the same ones. By utilizing both Excel and Python, we can perform a join between two arrays based on the common project IDs.


### 2020-CP:

- Projects with V1 Merge Requests that may or may not exist in V2

    | Projet |Total Nb of MR in V2 | Nb of common MR  (V1 and V2)| 
    |:------:|:-------------------:|:---------------------------:|
    | 213    |      39             |  0                          |
    | 214    |      31             |  0                          |

- Projects with V2 Merge Requests that may or may not exist in V1 

    | Projet | Total Nb of MR in V2 |Nb of common MR (V1 and V2)| 
    |:------:|:--------------------:|:-------------------------:|
    | 124    |      98              |0                          |
    | 1409   |      6               |0                          |

### 2021-CP:

- Projects with V1 Merge Requests that may or may not exist in V2

    | Projet | Total Nb of MR in V2| Nb of common MR (V1 and V2) |
    |:------:|:-------------------:|:---------------------------:|
    | 213    | 19                  | 0                           |
    | 214    | 26                  | 0                           |

- Projects with V2 Merge Requests that may or may not exist in V1
    | Projet | Total Nb of MR in V2 | Nb of common MR (V1 and V2) |
    |:------:|:-------------------:|:---------------------------:|
    | 88     | 23                  | 16                          |
    | 129    | 61                  | 37                          |
    | 156    | 35                  | 25                          |
    | 785    | 148                 | 116                         |
    | 873    | 34                  | 24                          |
    | 876    | 79                  | 44                          |
    | 877    | 47                  | 35                          |
    | 913    | 18                  | 12                          |
    | 1122   | 27                  | 18                          |
    | 1125   | 98                  | 58                          |
    | 1126   | 60                  | 27                          |
    | 1135   | 114                 | 66                          |
    | 1147   | 54                  | 46                          |
    | 1409   | 9                   | 0                           |
    | 1446   | 57                  | 44                          |
    | 1447   | 82                  | 50                          |
    | 1584   | 80                  | 57                          |
    | 1728   | 118                 | 0                           |
    | 1734   | 24                  | 0                           |
    | 1758   | 17                  | 0                           |

### 2020-MP:

- Projects with V1 Merge Requests that may or may not exist in V2

    *No Projects*

- Projects with V2 Merge Requests that may or may not exist in V1

    | Projet | Total Nb of MR in V2| Nb of common MR (V1 and V2) |
    |:------:|:-------------------:|:---------------------------:|
    | 213    | 39                  | 0                           |
    | 214    | 31                  | 0                           |
    | 1621   | 23                  | 0                           |

### 2021-MP:

- Projects with V1 Merge Requests that may or may not exist in V2  

    *No Projects*

- Projects with V2 Merge Requests that may or may not exist in V1

    | Projet | Total Nb of MR in V2| Nb of common MR (V1 and V2) |
    |:------:|:-------------------:|:---------------------------:|
    | 32     | 10                  | 6                           |
    | 89     | 28                  | 20                          |
    | 153    | 204                 | 130                         |
    | 172    | 9                   | 6                           |
    | 213    | 27                  | 0                           |
    | 214    | 41                  | 0                           |
    | 806    | 290                 | 194                         |
    | 850    | 88                  | 56                          |
    | 882    | 360                 | 250                         |
    | 1300   | 18                  | 17                          |
    | 1621   | 82                  | 0                           |
    | 1679   | 17                  | 0                           |
    | 1684   | 52                  | 0                           |
    | 1740   | 13                  | 0                           |
    | 1771   | 33                  | 0                           |

### 2020-DP:

- Projects with V1 Merge Requests that may or may not exist in V2
    
    *No Projects*

- Projects with V2 Merge Requests that may or may not exist in V1

    | Projet | Total Nb of MR in V2| Nb of common MR (V1 and V2) |
    |:------:|:-------------------:|:---------------------------:|
    | 82     | 209                 | 0                           |
    | 154    | 71                  | 0                           |
    | 669    | 2                   | 0                           |
    | 685    | 11                  | 0                           |
    | 1499   | 92                  | 0                           |

### 2021-DP

- Projects with V1 Merge Requests that may or may not exist in V2
    
    *No Projects*

- Projects with V2 Merge Requests that may or may not exist in V1

    | Projet | Total Nb of MR in V2| Nb of common MR (V1 and V2) |
    |:------:|:-------------------:|:---------------------------:|
    | 82     | 137                 | 0                           |
    | 143    | 237                 | 156                         |
    | 154    | 52                  | 0                           |
    | 685    | 26                  | 0                           |
    | 1144   | 642                 | 425                         |
    | 1304   | 25                  | 17                          |
    | 1499   | 87                  | 0                           |
    | 1501   | 58                  | 57                          |
    | 1649   | 27                  | 19                          |
    | 1732   | 32                  | 0                           |
    | 1763   | 158                 | 0                           |
    | 1765   | 23                  | 0                           |
    | 1800   | 8                   | 0                           |

*For the project 1501, the 58th merge request has been closed the 2021-09-16, a date the first version of the tool didn't read*

### 2020-PF:

- Projects with V1 Merge Requests that may or may not exist in V2
    
    *No Projects*

- Projects with V2 Merge Requests that may or may not exist in V1

    | Projet | Total Nb of MR in V2| Nb of common MR (V1 and V2) |
    |:------:|:-------------------:|:---------------------------:|
    | 776    | 25                  | 0                           |
    | 1631   | 1                   | 0                           |

### 2021-PF:

- Projects with V1 Merge Requests that may or may not exist in V2
    
    *No Projects*

- Projects with V2 Merge Requests that may or may not exist in V1

    | Projet | Total Nb of MR in V2| Nb of common MR (V1 and V2) |
    |:------:|:-------------------:|:---------------------------:|
    | 544    | 14                  | 11                          |
    | 545    | 16                  | 13                          |
    | 545    | 16                  | 13                          |
    | 765    | 15                  | 14                          |
    | 776    | 30                  | 0                           |
    | 789    | 149                 | 116                         |
    | 794    | 12                  | 4                           |
    | 851    | 9                   | 6                           |
    | 878    | 8                   | 7                           |
    | 1036   | 25                  | 18                          |
    | 1145   | 17                  | 11                          |
    | 1146   | 18                  | 14                          |
    | 1459   | 21                  | 17                          |
    | 1571   | 20                  | 17                          |
    | 1585   | 441                 | 312                         |
    | 1631   | 5                   | 0                           |
    | 1643   | 37                  | 24                          |
    | 1647   | 20                  | 14                          |
    | 1664   | 14                  | 10                          |
    | 1673   | 17                  | 0                           |
    | 1693   | 12                  | 0                           |
    | 1752   | 3                   | 0                           |
    | 1755   | 4                   | 0                           |
    | 1756   | 6                   | 0                           |

In these tables, we may find that some projects have more merge requests in V2 during 2021 than in V1. The reason behind this discrepancy is that V1 did not extract data after August 12th, while V2 covers a more recent time frame, including the entire year of 2021. Therefore, some merge requests from 2021 that were missed by V1 will be present in V2's data set, contributing to the difference in the number of merge requests for certain projects between the two versions.

## 3. Analyzing the new FPGA group

The table shows the number of merge requests for each project in 2020 and 2021, using the new version of the tool which includes the "FPGA" group.


Project | Nb Merge Requests (2020) | Nb Merge Requests (2021) |
|:-----:|:------------------------:|:-----:
| 1388  | 71                       | None
| 1389  | 15                       |12
| 1602  | 18                       |66
| 1603  | 11                       |31
| 1663  | None                     |17


The analysis of the results indicates that the new version of the tool, which includes the "FPGA" group, has captured data for certain projects in both 2020 and 2021. However, there are notable variations in the number of merge requests for different projects between the two years. Further investigation may be necessary to understand the underlying reasons behind these fluctuations.




